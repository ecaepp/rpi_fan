import RPi.GPIO as GPIO
from gpiozero import CPUTemperature
from math import floor
from time import sleep


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(32, GPIO.OUT)

GPIO.output(32, GPIO.LOW)

cpu = CPUTemperature()

# fan.start(min_spin)

old_temp = cpu.temperature



def start_fan():
    GPIO.output(32, GPIO.HIGH)
    print('starting fan')
    
    
def stop_fan():
    GPIO.output(32, GPIO.LOW)
    print('stopping fan')
 
    
def check_temp():
    cpu = CPUTemperature()
    return floor(cpu.temperature)


while True:
    
    temp = check_temp()
    print(f'{temp}')
    
    if temp > 65 :
        start_fan()
    else:
        print('fan off')
        stop_fan()
    
    sleep(10)
stop_fan()
print("done")